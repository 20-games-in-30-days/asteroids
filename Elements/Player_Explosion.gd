extends RigidBody2D

var asteroids_left := 2

func _ready():
	$Particles.color = Global.line_color
	$Particles.emitting = true


func destroy():
	if asteroids_left > 0:
		asteroids_left -= 1
	else:
		set_collision_layer_value(1, 0)


func _on_Sound_finished():
	queue_free()
