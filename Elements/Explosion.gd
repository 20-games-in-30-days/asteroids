extends Node2D

func _ready():
	$Particles.color = Global.line_color
	$Particles.emitting = true


func boom(size):
	if size == Asteroid.SMALL:
		$Small.play()
		
	elif size == Asteroid.MEDIUM:
		$Medium.play()
		
	elif size == Asteroid.LARGE:
		$Large.play()


func _on_Sound_finished():
	queue_free()
