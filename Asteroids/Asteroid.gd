extends RigidBody2D
class_name Asteroid

enum {SMALL, MEDIUM, LARGE}
@export var size := LARGE

signal broke


func _ready():
	$Line.width = Global.asteroid_line_weight
	$Line.default_color = Global.line_color


func set_velocity(base_vel: Vector2):
	var multiplier
	var direction := Vector2(randf_range(-1, 1), randf_range(-1, 1))
	
	if base_vel == Vector2.ZERO:
		multiplier = randf_range(Global.base_speed.x, Global.base_speed.y)
	else:
		multiplier = randf_range(Global.split_speed.x, Global.split_speed.y)
	
	linear_velocity = base_vel + (direction.normalized() * Global.speed_factor * multiplier)


func _physics_process(_delta):
	if global_position.x < 0:
			global_position += Vector2(Global.game_window_size.x, 0)
	elif global_position.x > Global.game_window_size.x:
		global_position -= Vector2(Global.game_window_size.x, 0)
	if global_position.y < 0:
		global_position += Vector2(0, Global.game_window_size.y)
	elif global_position.y > Global.game_window_size.y:
		global_position -= Vector2(0, Global.game_window_size.y)


func _on_Area2D_body_entered(body):
	queue_free()
	body.destroy()
	emit_signal("broke", size, global_position, linear_velocity)
