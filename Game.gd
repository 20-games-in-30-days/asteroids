extends CanvasLayer

@export var new_asteroids_per_level = 1

@export var asteroids_large := [] # (Array, Resource)
@export var asteroids_medium := [] # (Array, Resource)
@export var asteroids_small := [] # (Array, Resource)
@onready var player := preload("res://Elements/Player.tscn")
@onready var explosion := preload("res://Elements/Explosion.tscn")
@onready var player_explosion := preload("res://Elements/Player_Explosion.tscn")

var respawning := true
var level_ending := true
var game_over := true

func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), -15)
	for i in 4:
		create_asteroid(Asteroid.LARGE, _random_valid_vector2(), Vector2.ZERO)
		create_asteroid(Asteroid.MEDIUM, _random_valid_vector2(), Vector2.ZERO)
		create_asteroid(Asteroid.SMALL, _random_valid_vector2(), Vector2.ZERO)


func reset_game():
	for asteroid in $Asteroids.get_children():
		asteroid.queue_free()
	$UI.reset()
	next_level()
	spawn_player()
	game_over = false


func spawn_player():
	var p := player.instantiate()
	p.global_position = Global.game_window_size / 2
	var _d = p.connect("died", Callable(self, "_on_Player_died"))
	add_child(p)
	p.owner = self
	respawning = false


func next_level():
	for i in Global.asteroids_to_spawn:
		create_asteroid(Asteroid.LARGE, _random_valid_vector2(), Vector2.ZERO)
		level_ending = false


func create_asteroid(size, location: Vector2, velocity: Vector2):
	assert(size == Asteroid.SMALL or size == Asteroid.MEDIUM or size == Asteroid.LARGE)
	
	var asteroid: Asteroid
	if size == Asteroid.LARGE:
		asteroid = asteroids_large[randi() % asteroids_large.size()].instantiate()
	elif size == Asteroid.MEDIUM:
		asteroid = asteroids_medium[randi() % asteroids_medium.size()].instantiate()
	elif size == Asteroid.SMALL:
		asteroid = asteroids_small[randi() % asteroids_small.size()].instantiate()
	
	$Asteroids.add_child(asteroid)
	asteroid.set_velocity(velocity)
	asteroid.global_position = location
	var _d = asteroid.connect("broke", Callable(self, "_on_Asteroid_broke"))


func create_explosion(size, location: Vector2):
	var boom = explosion.instantiate()
	boom.global_position = location
	add_child(boom)
	boom.boom(size)


func _physics_process(_delta):
	if not level_ending and $Asteroids.get_child_count() == 0:
		_on_level_end()
	if game_over and Input.is_action_just_pressed("shoot"):
		reset_game()


func _on_level_end():
	level_ending = true
	$UI.on_level_end()
	await get_tree().create_timer(5.0).timeout
	$UI.on_level_start()
	next_level()


func _on_Asteroid_broke(size, position: Vector2, velocity: Vector2):
	await get_tree().process_frame
	$UI.asteroid_broke(size)
	create_explosion(size, position)
	
	if size > Asteroid.SMALL:
		create_asteroid(size - 1, position, velocity)
		create_asteroid(size - 1, position, velocity)
		if randi() % 3 == 1:
			create_asteroid(size - 1, position, velocity)


func _random_valid_vector2() -> Vector2:
	var vector = Vector2(randf_range(64, Global.game_window_size.x - 64), randf_range(64, Global.game_window_size.y - 64))
	while !respawning and (vector - $Player.transform.origin).length() < 512:
		vector = Vector2(randf_range(64, Global.game_window_size.x - 64), randf_range(64, Global.game_window_size.y - 64))
	
	return vector


func _on_Player_died(position: Vector2, velocity: Vector2):
	await get_tree().process_frame
	$UI.player_died()
	var boom := player_explosion.instantiate()
	respawning = true
	boom.global_position = position
	boom.linear_velocity = velocity / 2
	$Music_Fade_in.stop()
	$Music.stop()
	add_child(boom)
	await get_tree().create_timer(5.0).timeout
	$Music_Fade_in.play()
	if $UI.player_lives_left >= 0:
		spawn_player()
	else:
		$UI.game_over()
		game_over = true


func _on_Music_Fade_in_finished():
	if not respawning:
		$Music.play(0)
