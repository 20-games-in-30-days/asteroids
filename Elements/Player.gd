extends CharacterBody2D

@export var speed := 200
@export var rotation_speed := 1.5
@export var friction := 0.1
@export var shot_speed := 200
@export var shot_cooldown := 0.1
@export var player_bullet: Resource

signal died

var vel := Vector2.ZERO
var boosting := false
var can_shoot := true
var invincible := true

func _ready():
	$Line.width = Global.ship_line_weight
	$Line.default_color = Global.line_color

	$Thrust.width = Global.asteroid_line_weight
	$Thrust.default_color = Global.line_color
	await get_tree().create_timer(2.0).timeout
	invincible = false


func _physics_process(delta):
	var rotation_dir = 0
	var add_velocity = Vector2.ZERO
	
	# Movement
	if Input.is_action_pressed('right'):
		rotation_dir += Input.get_action_strength("right")
	if Input.is_action_pressed('left'):
		rotation_dir -= Input.get_action_strength("left")
	if Input.is_action_pressed('up'):
		add_velocity += transform.x * speed * Global.speed_factor
		if not boosting:
			boosting = true
			$ThrustAnimation.play("Thrust")
			$ThrustSound.stream_paused = false
	elif boosting:
		boosting = false
		$ThrustAnimation.play("Idle")
		$ThrustSound.stream_paused = true

	rotation += rotation_dir * rotation_speed * delta
	set_velocity(vel * (1 - friction) + add_velocity)
	move_and_slide()
	vel = velocity
	
	# Warp
	if Input.is_action_just_pressed("warp"):
		global_position = Vector2(randf_range(64, Global.game_window_size.x - 64), randf_range(64, Global.game_window_size.y - 64))
	
	# Screen Wrap
	screen_wrap()
	
	# Shooting
	if Input.is_action_pressed("shoot") and can_shoot:
		can_shoot = false
		$Shot_Timer.start(shot_cooldown / Global.speed_factor)
		create_bullet(transform.x)

	if Input.is_action_just_pressed("special") and Global.specials_left > 0:
		Global.specials_left -= 1
		for i in 50:
			var dir1 = deg_to_rad((i * 3.6) + rotation_degrees)
			var dir2 = deg_to_rad((-i * 3.6) + rotation_degrees)
			var dir3 = deg_to_rad((i * 3.6) + 180 + rotation_degrees)
			var dir4 = deg_to_rad((-i * 3.6) + 180 + rotation_degrees)
			create_bullet(Vector2(cos(dir1), sin(dir1)))
			create_bullet(Vector2(cos(dir2), sin(dir2)))
			create_bullet(Vector2(cos(dir3), sin(dir3)))
			create_bullet(Vector2(cos(dir4), sin(dir4)))
			await get_tree().process_frame


func create_bullet(direction: Vector2):
	var bullet = player_bullet.instantiate()
	$ShootSound.play()
	owner.add_child(bullet)
	bullet.linear_velocity = direction * shot_speed + vel
	bullet.global_position = global_position + (direction * 38)


func screen_wrap():
	if global_position.x < 0:
			global_position += Vector2(Global.game_window_size.x, 0)
	elif global_position.x > Global.game_window_size.x:
		global_position -= Vector2(Global.game_window_size.x, 0)
	if global_position.y < 0:
		global_position += Vector2(0, Global.game_window_size.y)
	elif global_position.y > Global.game_window_size.y:
		global_position -= Vector2(0, Global.game_window_size.y)


func _on_Shot_Timer_timeout():
	can_shoot = true


func _on_Area2D_body_entered(_body):
	if not invincible:
		emit_signal("died", global_position, vel)
		queue_free()
