extends RigidBody2D

@export var spin_speed := Vector2(10, 20)


func _ready():
	$Line.width = Global.asteroid_line_weight
	$Line.default_color = Global.line_color
	if randf_range(0, 1) < 0.5:
		angular_velocity = randf_range(spin_speed.x, spin_speed.y)
	else:
		angular_velocity = -randf_range(spin_speed.x, spin_speed.y)


func _physics_process(_delta):
	if global_position.x < 0:
			global_position += Vector2(Global.game_window_size.x, 0)
	elif global_position.x > Global.game_window_size.x:
		global_position -= Vector2(Global.game_window_size.x, 0)
	if global_position.y < 0:
		global_position += Vector2(0, Global.game_window_size.y)
	elif global_position.y > Global.game_window_size.y:
		global_position -= Vector2(0, Global.game_window_size.y)


func destroy():
	queue_free()


func _on_Timer_timeout():
	queue_free()
