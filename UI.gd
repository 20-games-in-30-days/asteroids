extends Node2D

@export var score := 0
@export var player_lives_left := 3
@export var level := 0


func _ready():
	$Ship_1.width = Global.ship_line_weight
	$Ship_2.width = Global.ship_line_weight
	$Ship_3.width = Global.ship_line_weight
	$Ship_1.default_color = Global.line_color
	$Ship_2.default_color = Global.line_color
	$Ship_3.default_color = Global.line_color
	$Special_1.width = Global.asteroid_line_weight
	$Special_2.width = Global.asteroid_line_weight
	$Special_1.default_color = Global.line_color
	$Special_2.default_color = Global.line_color
	$Score.add_theme_color_override("font_color", Global.line_color)
	$Game_Text.add_theme_color_override("font_color", Global.line_color)


func asteroid_broke(size):
	add_score((size + 1) * 50)


func add_score(value: int):
	score += value
	$Score.text = str(score)


func reset():
	level = 0
	score = 0
	player_lives_left = 3
	Global.asteroids_to_spawn = 3
	Global.specials_left = 2
	Global.speed_factor = 1
	$Score.text = str(score)
	$Game_Text.visible = false
	draw_player_lives()


func _process(_delta):
	$Special_1.visible = Global.specials_left >= 1
	$Special_2.visible = Global.specials_left >= 2


func on_level_end():
	level += 1
	Global.asteroids_to_spawn = 3 + (level % 10)
	Global.speed_factor = (level / 10) + 1
	
	if Global.speed_factor == 1 and level % 2 and Global.specials_left < Global.max_specials:
		Global.specials_left += 1
	elif Global.speed_factor >= 2 and Global.specials_left < Global.max_specials:
		Global.specials_left += 1
		
	# Award a life for each level cleared
	if player_lives_left < 3:
		player_lives_left += 1
		draw_player_lives()
		$Game_Text.text = "Level " + str(level) + " Cleared"
	else:
		# Level clear bonus!
		add_score(5000)
		$Game_Text.text = "Level " + str(level) + " Cleared\nMax Lives Bonus!"
	$Game_Text.visible = true


func game_over():
	$Game_Text.text = "Game Over\nFinal Score: " + str(score)
	$Game_Text.visible = true


func on_level_start():
	$Game_Text.visible = false


func player_died():
	player_lives_left -= 1
	draw_player_lives()


func draw_player_lives():
	$Ship_1.visible = player_lives_left >= 1
	$Ship_2.visible = player_lives_left >= 2
	$Ship_3.visible = player_lives_left >= 3
