extends Node2D

@export var game_window_size = Vector2(1600, 900)
@export var line_color: Color
@export var asteroid_line_weight: float
@export var ship_line_weight: float

@export var asteroids_to_spawn = 3
@export var base_speed := Vector2.ONE
@export var split_speed := Vector2.ONE

@export var speed_factor := 1
@export var specials_left := 2
@export var max_specials := 2
